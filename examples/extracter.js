const Unpack = require('../lib/unpack.js')
const UnpackSync = Unpack.Sync
const fs = require('fs')
const path = require('path')

const dir = '/tmp';
const unpack = new UnpackSync({ cwd: dir});
unpack.end(fs.readFileSync('../test/fixtures/tars/linuxstart-20120111.tar'));